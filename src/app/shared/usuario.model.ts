export class Usuario {
    nombre: string;
    apellido: string;
    movil: number;

    constructor(nombre:string, apellido:string, movil:number){
        this.nombre = nombre;
        this.apellido = apellido;
        this.movil = movil;
    }
}