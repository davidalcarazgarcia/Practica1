import { Component } from '@angular/core';

@Component({
  selector: 'dag-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
